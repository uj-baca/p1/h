#include <iostream>

#define nullptr NULL

template<typename, typename>
struct is_same {
    static const bool value = false;
};
template<typename T>
struct is_same<T, T> {
    static const bool value = true;
};

using namespace std;

struct EndOfInputException {
};

struct ID {
    char id1, id2, id3;

    ID():id1('\0'), id2('\0'), id3('\0') {}

    friend istream &operator>>(istream &input, ID &id) {
        input >> skipws >> id.id1;
        if (id.id1 == '$') {
            throw EndOfInputException();
        }
        input >> id.id2 >> id.id3;
        return input;
    }

    friend ostream &operator<<(ostream &output, ID &id) {
        output << id.id1 << id.id2 << id.id3;
        return output;
    }

    bool operator==(const ID &id) {
        return id1 == id.id1 && id2 == id.id2 && id3 == id.id3;
    }

    bool operator>(const ID &id) {
        if (id1 > id.id1) {
            return true;
        } else if (id1 < id.id1) {
            return false;
        } else {
            if (id2 > id.id2) {
                return true;
            } else if (id2 < id.id2) {
                return false;
            } else {
                return id3 > id.id3;
            }
        }
    }

    bool operator<(const ID &id) {
        if (id1 > id.id1) {
            return false;
        } else if (id1 < id.id1) {
            return true;
        } else {
            if (id2 > id.id2) {
                return false;
            } else if (id2 < id.id2) {
                return true;
            } else {
                return id3 < id.id3;
            }
        }
    }

    bool operator!=(const ID &id) {
        return id1 != id.id1 || id2 != id.id2 || id3 != id.id3;
    }
};


template<typename T>
struct List {
    struct Element {
        T *value;
        Element *prev, *next;

        explicit Element(T *value, Element *prev = nullptr) {
            this->value = value;
            this->prev = prev;
            this->next = nullptr;
        }

        bool operator>(Element *another) {
            return this->value > another->value;
        }

        bool operator<(Element *another) {
            return this->value < another->value;
        }

        bool operator==(Element *another) {
            return this->value == another->value;
        }

        bool operator!=(Element *another) {
            return this->value != another->value;
        }

        bool operator>=(Element *another) {
            return this->value >= another->value;
        }

        bool operator<=(Element *another) {
            return this->value <= another->value;
        }
    };

    Element *firstElement, *lastElement;
    ID id;

    List():firstElement(nullptr), lastElement(nullptr) {}

    friend istream &operator>>(istream &input, List<T> *l) {
        if (is_same<T, int>::value) {
            char character;
            while (true) {
                cin >> skipws >> character >> noskipws;
                int value = 0;
                while (true) {
                    if (character == ' ') {
                        break;
                    }
                    if (character == 'R') {
                        break;
                    }
                    if (character == 'S') {
                        break;
                    }
                    value *= 10;
                    value += (character - '0');
                    input >> noskipws >> character;
                }
                if (character == 'R') {
                    l->reverse();
                    break;
                }
                if (character == 'S') {
                    break;
                }
                l->append(value);
            }
        }
        return input;
    }

    friend ostream &operator<<(ostream &output, List<T> &list) {
        Element *tmp = list.firstElement;
        if (tmp) {
            while (true) {
                output << *tmp->value << ' ';
                if (!tmp->next) {
                    break;
                }
                tmp = tmp->next;
            }
        }
        output << endl;
        return output;
    }

    void append(T *value) {
        if (firstElement != nullptr) {
            lastElement->next = new Element(value, lastElement);
            lastElement = lastElement->next;
        } else {
            lastElement = firstElement = new Element(value);
        }
    }

    void append(T &value) {
        if (firstElement != nullptr) {
            lastElement->next = new Element(new T(value), lastElement);
            lastElement = lastElement->next;
        } else {
            lastElement = firstElement = new Element(new T(value));
        }
    }

    void sort() {
        Element *oldList = firstElement;
        Element *newList = nullptr;
        Element *newNext = nullptr;
        Element *lowest = nullptr;
        while (oldList != nullptr) {
            Element *walk = oldList;
            lowest = walk;
            while (walk) {
                if (((List<int> *) lowest->value)->id > ((List<int> *) walk->value)->id) {
                    lowest = walk;
                }
                walk = walk->next;
            }
            if (lowest->prev) {
                oldList = lowest->next;
            } else {
                lowest->prev->next = lowest->next;
            }

            if (!lowest->next && lowest->prev) {
                lowest->prev->next = nullptr;
            } else if (lowest->next) {
                lowest->next->prev = lowest->prev;
            }

            if (!newNext) {
                newList = lowest;
                newNext = newList;
            } else {
                newNext->next = lowest;
                lowest->prev = newNext;
                newNext = lowest;
            }
            firstElement = newList;
            lastElement = firstElement;
            while (lastElement->next) {
                lastElement->next;
            }
        }
    }

    void reverse() {
        if (firstElement) {
            Element *first = firstElement;
            firstElement = lastElement;
            Element *next;
            Element *prev = nullptr;
            Element *temp = firstElement;
            while (lastElement != first) {
                next = lastElement->prev;
                temp->next = next;
                temp->prev = prev;
                prev = temp;
                temp = temp->next;
                lastElement = next;
            }
            temp->next = lastElement;
            lastElement->prev = temp;
            lastElement->next = nullptr;
        }
    }

    void execute(int start, int freq, bool begin) {
        if (begin) {
            Element *walk = firstElement;
            for (int i = 0; i < start - 1 && walk->next; i++) {
                walk = walk->next;
            }
            int help = 1;
            while (true) {
                if (!(help++) % freq) {
                    if (!walk->prev && !walk->next) {
                        firstElement = lastElement = nullptr;
                    } else if (!walk->prev) {
                        walk->next->prev = nullptr;
                        firstElement = firstElement->next;
                    } else if (!walk->next) {
                        walk->prev->next = nullptr;
                        lastElement = lastElement->prev;
                    } else {
                        walk->prev->next = walk->next;
                        walk->next->prev = walk->prev;
                    }
                    delete walk;
                }
                if (!walk->next) {
                    break;
                }
                walk = walk->next;
            }
        } else {
            Element *walk = lastElement;
            for (int i = 0; i < start - 1 && walk->prev; i++) {
                walk = walk->prev;
            }
            int help = 1;
            while (true) {
                if (!(help++) % freq) {
                    if (!walk->next && !walk->prev) {
                        firstElement = lastElement = nullptr;
                    } else if (!walk->next) {
                        walk->prev->next = nullptr;
                        lastElement = lastElement->prev;
                    } else if (!walk->prev) {
                        walk->prev->next = nullptr;
                        firstElement = firstElement->next;
                    } else {
                        walk->prev->next = walk->next;
                        walk->next->prev = walk->prev;
                    }
                    delete walk;
                }
                if (!walk->prev) {
                    break;
                }
                walk = walk->prev;
            }
        }
    }
};

int main() {
    List<List<int> > *mainList;
    int mainCount;
    cin >> mainCount;
    for (int i = 0; i < mainCount; i++) {
        mainList = new List<List<int> >;
        try {
            while (true) {
                auto *tmpList = new List<int>();
                cin >> tmpList->id;
                cin >> tmpList;
                mainList->append(tmpList);
            }
        } catch (EndOfInputException&) {}
         mainList->sort();
        List<List<int> >::Element *el = mainList->firstElement;
        List<int> *l;
        while (true) {
            l = el->value;
            cout << l->id << ' ' << *l;
            if (!el->next) {
                break;
            }
            el = el->next;
        }
        int count;
        cin >> count;
        for (int j = 0; j < count; j++) {
            cout << "*** " << j << " ***" << endl;
            cin >> skipws;
            ID id;
            cin >> id;
            char what;
            int start, freq, begin;
            cin >> what;
            freq = what == 'D' ? 2 : 3;
            cin >> what;
            if (what == 'F') start = 1; else if (what == 'S') start = 2; else start = 3;
            cin >> what;
            begin = what == 'B';
            List<List<int> >::Element *el = mainList->firstElement;
            while (true) {
                List<int> *list = el->value;
                if (list->id == id) {
                    list->execute(start, freq, begin);
                    break;
                }
                if (!el->next) {
                    break;
                }
                el = el->next;
            }
        }
        el = mainList->firstElement;
        while (true) {
            l = el->value;
            cout << l->id << ' ' << *l;
            if (!el->next) {
                break;
            }
            el = el->next;
        }
        delete el;
        delete l;
    }
}
